<?php
require_once "spyc/Spyc.php";
require_once 'CheckOS.php';
require_once 'CleanerInterface.php';
require_once 'LifeSource.php';
require_once 'LifeGenerator.php';
require_once 'LifeViewer.php';

LifeViewer::play(LifeSource::getStartLife($argv[1] ?? 'life.yml'));
