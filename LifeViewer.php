<?php

class LifeViewer implements CleanerInterface
{
    /**
     * Clean console in windows.
     */
    public static function windowsClearScreen()
    {
        for ($i = 0; $i < 1000; $i++) echo "\r\n";
    }

    /**
     * Clean console in linux.
     */
    public static function defaultClearScreen()
    {
        system('clear');
    }

    /**
     * GameOver message.
     *
     * @param $curGen
     * @return string
     */
    protected static function gameOverMessage($curGen)
    {
        return "\r\nGame over.\r\n" . (LifeGenerator::isAlive($curGen) ? 'Stable state.' : 'No more life here.');
    }

    /**
     * Rewrite start generation to accepted format.
     *
     * @param $gen
     * @param $alive
     * @param $dead
     * @return array
     */
    public static function binaryLife($gen, $alive, $dead)
    {
        $formattedLife = array();

        foreach ($gen as $key => $row) {
            $formattedLife[$key] = str_replace(array($alive, $dead), array('1', '0'), $row);
        }
        return $formattedLife;
    }

    /**
     * View life generations.
     *
     * @param $data
     * @param int $genNumber
     */
    public static function play($data, $genNumber = 0)
    {
        $curGen = self::binaryLife($data['life'], $data['alive'], $data['dead']);
        $nextGen = array();

        $clearScreen = CheckOS::getOS() . 'ClearScreen';

        while (LifeGenerator::isActive($curGen, $nextGen) && LifeGenerator::isAlive($curGen)) {
            sleep(1);
            self::$clearScreen();

            if ($nextGen) {
                $curGen = $nextGen;
                $nextGen = array();
            }

            $height = count($curGen);
            for ($i = 0; $i < $height; $i++) {
                $nextGen[$i] = '';
                $width = strlen($curGen[$i]);
                for ($j = 0; $j < $width; $j++) {
                    echo $curGen[$i][$j] ? $data['alive'] : $data['dead'];
                    $nextGen[$i] .= (int)LifeGenerator::getNextCellState($curGen, $i, $j);
                }
                echo "\r\n";
            }
            echo "\r\nGeneration: " . ++$genNumber;
        }
        echo self::gameOverMessage($curGen);
    }
}
