<?php

final class checkOS
{
    private static $os;

    private function __construct()
    {
    }

    private function __clone()
    {
    }

    /**
     * Return OS type.
     * Each type must be handled by CleanerInterface.
     *
     * @return string
     */
    public static function getOS()
    {
        if (empty(self::$os)) {
            self::$os = strtoupper(substr(PHP_OS, 0, 3)) === 'WIN' ? 'windows' : 'default';
        }

        return self::$os;
    }
}
