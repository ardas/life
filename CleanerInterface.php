<?php

interface CleanerInterface
{
    public static function windowsClearScreen();

    public static function defaultClearScreen();
}