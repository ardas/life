<?php

class LifeGenerator
{
    /**
     * Check neighbors states. Return next state of the current cell according to rules.
     *
     * @param array $curGen
     * @param $i
     * @param $j
     * @return bool
     */
    public static function getNextCellState(array $curGen, $i, $j)
    {
        $neighbors = array_sum(array(
            $curGen[$i - 1][$j - 1] && $j > 0 ? $curGen[$i - 1][$j - 1] : 0,
            $curGen[$i - 1][$j] ?? 0,
            $curGen[$i - 1][$j + 1] ?? 0,
            $curGen[$i][$j - 1] && $j > 0 ? $curGen[$i][$j - 1] : 0,
            $curGen[$i][$j + 1] ?? 0,
            $curGen[$i + 1][$j - 1] && $j > 0 ? $curGen[$i + 1][$j - 1] : 0,
            $curGen[$i + 1][$j] ?? 0,
            $curGen[$i + 1][$j + 1] ?? 0,
        ));

        return $curGen[$i][$j] ? 2 == $neighbors || 3 == $neighbors : 3 == $neighbors;
    }

    /**
     * Check if next generation is equal to the current generation.
     *
     * @param array $curGen
     * @param array $nextGen
     * @return bool
     */
    public static function isActive(array $curGen, array $nextGen)
    {
        return $curGen !== $nextGen;
    }

    /**
     * Check if generation contains life.
     *
     * @param array $gen
     * @return bool
     */
    public static function isAlive(array $gen)
    {
        foreach ($gen as $row) {
            if (false !== strpos($row, '1')) {
                return true;
            }
        }
        return false;
    }
}
