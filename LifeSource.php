<?php

final class LifeSource
{
    private static $startLife = array();

    private function __construct()
    {
    }

    private function __clone()
    {
    }

    /**
     * Read start data from file.
     *
     * @param $fileName
     * @return array
     */
    public static function getStartLife($fileName)
    {
        if (empty(self::$startLife)) {

            if (!file_exists($fileName)) {
                echo 'File not found.';
                exit;
            }
            $data = Spyc::YAMLLoad($fileName);

            self::$startLife = array(
                'alive' => $data['alive'] ?? 1,
                'dead'  => $data['dead'] ?? 0,
                'life'  => $data['life'] ?? array(),
            );
        }

        return self::$startLife;
    }
}
